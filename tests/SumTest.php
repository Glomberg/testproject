<?php


class SumTest extends PHPUnit\Framework\TestCase
{
    public function testGetSum () : void
    {
        $sumObj = new Sum();
        $sum = $sumObj->getSum( 3, 5 );
        $this->assertEquals( 8, $sum );
    }
}
